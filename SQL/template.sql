CREATE TABLE `template` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `routine_id` int UNIQUE NOT NULL,
  `template_title` varchar(255) NOT NULL,
  `template_content` text NOT NULL,
  `update_date` datetime NOT NULL

);
