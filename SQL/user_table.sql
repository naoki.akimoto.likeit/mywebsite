CREATE DATABASE my_web_site DEFAULT CHARACTER SET utf8;

CREATE TABLE `user` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `login_id` varchar(255) UNIQUE NOT NULL,
  `user_nm` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL


);
