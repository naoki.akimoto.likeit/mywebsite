CREATE TABLE `routine` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `routine_nm` varchar(255) NOT NULL,
  `period` varchar(255),
  `effects` text,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `status_id` varchar(255) NOT NULL

);
