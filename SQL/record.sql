CREATE TABLE `record` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `routine_id` int NOT NULL,
  `record_title` varchar(255) NOT NULL ,
  `record_content` text NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL

);
