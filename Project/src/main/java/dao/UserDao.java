package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.UserDataBeans;

public class UserDao {

	
	 public UserDataBeans findByLoginInfo(String loginId, String password) {
		 
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            int idData = rs.getInt("id"); 
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("user_nm");
            return new UserDataBeans(idData,loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	 }


    /**
     * 全てのユーザ情報を取得する
     * @return
     */
//    public List<UserDataBeans> findAll() {
//        Connection conn = null;
//        List<UserDataBeans> userList = new ArrayList<UserDataBeans>();
//
//        try {
//            // データベースへ接続
//            conn = DBManager.getConnection();
//
//            // SELECT文を準備
//            // TODO: 未実装：管理者以外を取得するようSQLを変更する
//            String sql = "SELECT * FROM user";
//
//             // SELECTを実行し、結果表を取得
//            Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.executeQuery(sql);
//
//            // 結果表に格納されたレコードの内容を
//            // Userインスタンスに設定し、ArrayListインスタンスに追加
//            while (rs.next()) {
//                int id = rs.getInt("id");
//                String loginId = rs.getString("login_id");
//                String name = rs.getString("user_nm");
//                String password = rs.getString("password");
//                String createDate = rs.getString("create_date");
//                String updateDate = rs.getString("update_date");
//                UserDataBeans user = new UserDataBeans(id, loginId, name, password, createDate, updateDate);
//
//                userList.add(user);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//            return null;
//        } finally {
//            // データベース切断
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                    return null;
//                }
//            }
//        }
//        return userList;
//    }
    
    public UserDataBeans confirmUserId(String loginId) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }
            int id = rs.getInt("id");

            return new UserDataBeans(id);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

	public void insertUser(String loginId, String password, String name) {
	    Connection conn = null;
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "INSERT INTO user (login_id, user_nm, password,create_date, update_date) VALUES ( ?, ?, ?, now(), now())";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, loginId);
	        pStmt.setString(2, name);
	        pStmt.setString(3, password);
	        
	        pStmt.executeUpdate();       
	        
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	
	
	public UserDataBeans findUserData(String id) {
        Connection conn = null;
        int Id = Integer.parseInt(id);
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, Id);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            int idDate = rs.getInt("id");
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("user_nm");
            String passwordData = rs.getString("password");
            String createDateData = rs.getString("create_date");
            String updateDateData = rs.getString("update_date");

            return new UserDataBeans(idDate, loginIdData, nameData, passwordData, createDateData, updateDateData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
	
	public void updateUserData(String name, String loginId) {
	    Connection conn = null;
        
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "UPDATE user SET name = ?, update_date = now() WHERE login_id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, name);
	        pStmt.setString(2, loginId);
        
	        pStmt.executeUpdate();
	       	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	public void updateUserData(String password, String name, String loginId) {
	    Connection conn = null;
        
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "UPDATE user SET password = ?, user_nm = ?, update_date = now() WHERE login_id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, password);
	        pStmt.setString(2, name);
	        pStmt.setString(3, loginId);
        
	        pStmt.executeUpdate();
	       	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	public void deleteUserData(String loginId) {
	    Connection conn = null;

	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "DELETE FROM user WHERE login_id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, loginId);
	      
	        pStmt.executeUpdate();

	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
}



