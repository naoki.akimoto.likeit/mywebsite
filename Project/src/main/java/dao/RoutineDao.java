package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.RoutineDataBeans;

public class RoutineDao {
	
	//ステータスの習慣を全て入手
	public List<RoutineDataBeans> findRoutineByStatus(int userId, int statusId) {
		
		Connection conn = null;
		List<RoutineDataBeans> routineStatusList = new ArrayList<RoutineDataBeans>();
		
	    try {
	    	
	    	// データベースへ接続
	    	conn = DBManager.getConnection();
		
			// SELECT文を準備
			String sql = "SELECT * FROM routine WHERE user_id = ? AND status_id = ?";
		
			// SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, userId);
	        pStmt.setInt(2, statusId);
	        ResultSet rs = pStmt.executeQuery();

			
			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
			// 必要なデータのみインスタンスのフィールドに追加
			int idData = rs.getInt("id");
			int userIdData = rs.getInt("user_id");
			int statusIdData = rs.getInt("status_id");
			String routineNmData = rs.getString("routine_nm");
			String periodData = rs.getString("period");
			String effectsData = rs.getString("effects");
			String createDateData = rs.getString("create_date");
			String updateDateData = rs.getString("update_date");
			
			RoutineDataBeans routine = new RoutineDataBeans(idData, userIdData, statusIdData, routineNmData, periodData, effectsData, createDateData, updateDateData);
	
			routineStatusList.add(routine);
	        
			}
		      
	    } catch (SQLException e) {
	    	e.printStackTrace();
	    	return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	    return routineStatusList;
	}
	
	
	
	public void insertRoutine(String routineName, String period, String effects, int userId, int statusId) {
	    Connection conn = null;
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "INSERT INTO routine (routine_nm, period, effects, create_date, update_date, user_id, status_id) VALUES ( ?, ?, ?, now(), now(), ?, ?)";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, routineName);
	        pStmt.setString(2, period);
	        pStmt.setString(3, effects);
	        pStmt.setInt(4, userId);
	        pStmt.setInt(5, statusId);

	        pStmt.executeUpdate();       
	        
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}

	public RoutineDataBeans findRoutineData(String id) {
	    Connection conn = null;
	    int Id = Integer.parseInt(id);
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();
	
	        // SELECT文を準備
	        String sql = "SELECT * FROM routine WHERE id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, Id);
	        ResultSet rs = pStmt.executeQuery();
	
	        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	        if (!rs.next()) {
	            return null;
	        }
	
	        // 必要なデータのみインスタンスのフィールドに追加
	        int idData = rs.getInt("id");
	        int userIdData = rs.getInt("user_id");
	        int statusIdData = rs.getInt("status_id");
	        String routineNmData = rs.getString("routine_nm");
	        String periodData = rs.getString("period");
	        String effectsData = rs.getString("effects");
	        String createDateData = rs.getString("create_date");
	        String updateDateData = rs.getString("update_date");
	
	        return new RoutineDataBeans(idData, userIdData, statusIdData, routineNmData, periodData, effectsData, createDateData, updateDateData);
	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }
	}
	
	public RoutineDataBeans findRoutineDataByIntId(int id) {
	    Connection conn = null;
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();
	
	        // SELECT文を準備
	        String sql = "SELECT * FROM routine WHERE id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, id);
	        ResultSet rs = pStmt.executeQuery();
	
	        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	        if (!rs.next()) {
	            return null;
	        }
	
	        // 必要なデータのみインスタンスのフィールドに追加
	        int idData = rs.getInt("id");
	        int userIdData = rs.getInt("user_id");
	        int statusIdData = rs.getInt("status_id");
	        String routineNmData = rs.getString("routine_nm");
	        String periodData = rs.getString("period");
	        String effectsData = rs.getString("effects");
	        String createDateData = rs.getString("create_date");
	        String updateDateData = rs.getString("update_date");
	
	        return new RoutineDataBeans(idData, userIdData, statusIdData, routineNmData, periodData, effectsData, createDateData, updateDateData);
	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }
	}

	public void updateRoutineData(int id, int statusId, String routineNm, String period, String effects) {
	    Connection conn = null;
//	    int Id = Integer.parseInt(id);
	    
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "UPDATE routine SET status_id = ?, routine_nm = ?, period = ?, effects = ?, update_date = now() WHERE id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, statusId);
	        pStmt.setString(2, routineNm);
	        pStmt.setString(3, period);
	        pStmt.setString(4, effects);
	        pStmt.setInt(5, id);
	    
	        pStmt.executeUpdate();
	       	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	public void deleteroutineData(String id) {
	    Connection conn = null;
	    int Id = Integer.parseInt(id);


	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "DELETE FROM routine WHERE id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, Id);
	      
	        pStmt.executeUpdate();

	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	
}


	
	