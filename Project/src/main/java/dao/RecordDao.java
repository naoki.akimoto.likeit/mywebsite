package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.RecordDataBeans;

public class RecordDao {
	
	//ステータスの習慣を全て入手
	public List<RecordDataBeans> findRecordLsit(String routineId) {
		
		Connection conn = null;
	    int RoutineId = Integer.parseInt(routineId);
		
		List<RecordDataBeans> recordList = new ArrayList<RecordDataBeans>();
		
	    try {
	    	
	    	// データベースへ接続
	    	conn = DBManager.getConnection();
		
			// SELECT文を準備
			String sql = "SELECT * FROM record WHERE routine_id = ? ORDER BY create_date ASC";
		
			// SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, RoutineId);
	        ResultSet rs = pStmt.executeQuery();

			
			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
			// 必要なデータのみインスタンスのフィールドに追加
			int idData = rs.getInt("id");
			int routineIdData = rs.getInt("routine_id");
			String recordTitleData = rs.getString("record_title");
			String recordContentData = rs.getString("record_content");
			String createDateData = rs.getString("create_date");
			String updateDateData = rs.getString("update_date");
			
			RecordDataBeans record = new RecordDataBeans(idData, routineIdData, recordTitleData, recordContentData, createDateData, updateDateData);
	
			recordList.add(record);
	        
			}
		      
	    } catch (SQLException e) {
	    	e.printStackTrace();
	    	return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	    return recordList;
	}
	
	
	
	public void insertRecord(int routineId, String recordTitle, String recordContent) {
	    Connection conn = null;
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "INSERT INTO record (routine_id, record_title, record_content, create_date, update_date) VALUES ( ?, ?, ?, now(), now())";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, routineId);
	        pStmt.setString(2, recordTitle);
	        pStmt.setString(3, recordContent);

	        pStmt.executeUpdate();       
	        
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}

	public RecordDataBeans findRecordData(String id) {
	    Connection conn = null;
	    int Id = Integer.parseInt(id);
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();
	
	        // SELECT文を準備
	        String sql = "SELECT * FROM record WHERE id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, Id);
	        ResultSet rs = pStmt.executeQuery();
	
	        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	        if (!rs.next()) {
	            return null;
	        }
	
	        // 必要なデータのみインスタンスのフィールドに追加
	        int idData = rs.getInt("id");
	        int routineIdData = rs.getInt("routine_id");
	        String recordTitleData = rs.getString("record_title");
	        String recordContentData = rs.getString("record_content");
	        String createDateData = rs.getString("create_date");
	        String updateDateData = rs.getString("update_date");
	
	        return new RecordDataBeans(idData, routineIdData, recordTitleData, recordContentData, createDateData, updateDateData); 
	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }
	}

	public void updateRecordData(int id, String recordTitle, String recordContent) {
	    Connection conn = null;
//		    int Id = Integer.parseInt(id);
	    
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "UPDATE record SET record_title = ?, record_content = ?, update_date = now() WHERE id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, recordTitle);
	        pStmt.setString(2, recordContent);
	        pStmt.setInt(3, id);

	    
	        pStmt.executeUpdate();
	       	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	public void deleteRecordData(int id) {
	    Connection conn = null;
//	    int Id = Integer.parseInt(id);

	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "DELETE FROM record WHERE id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, id);
	      
	        pStmt.executeUpdate();

	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	
}

