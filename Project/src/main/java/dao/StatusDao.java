package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.StatusDataBeans;

public class StatusDao {

	public void insertStatus(int userId, String statusName) {
	    Connection conn = null;
//	    int UserId = Integer.parseInt(userId);
	    
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "INSERT INTO status (user_id, status_nm, update_date) VALUES ( ?, ?, now())";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, userId);
	        pStmt.setString(2, statusName);

	        pStmt.executeUpdate();       
	        
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	public StatusDataBeans findStatusData(String userId) {
	    Connection conn = null;
	    int UserId = Integer.parseInt(userId);
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();
	
	        // SELECT文を準備
	        String sql = "SELECT * FROM status WHERE user_id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, UserId);
	        ResultSet rs = pStmt.executeQuery();
	
	        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	        if (!rs.next()) {
	            return null;
	        }
	
	        // 必要なデータのみインスタンスのフィールドに追加
	        int idData = rs.getInt("id");
	        int userIdData = rs.getInt("user_id");
	        String statusNameData = rs.getString("status_nm");
	        String updateDateData = rs.getString("update_date");
	
	        return new StatusDataBeans(idData, userIdData, statusNameData, updateDateData);
	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }
	}
	
	//ステータスの習慣を全て入手
	public List<StatusDataBeans> findStatusListData(int userId) {
		
		Connection conn = null;
		List<StatusDataBeans> statusList = new ArrayList<StatusDataBeans>();
		
	    try {
	    	
	    	// データベースへ接続
	    	conn = DBManager.getConnection();
		
			// SELECT文を準備
			String sql = "SELECT * FROM status WHERE user_id = ?";
		
			// SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, userId);
	        ResultSet rs = pStmt.executeQuery();
			
			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
			// 必要なデータのみインスタンスのフィールドに追加
			int idData = rs.getInt("id");
			int userIdData = rs.getInt("user_id");
			String statusNameData = rs.getString("status_nm");
			String updateDateData = rs.getString("update_date");
			
			StatusDataBeans status = new StatusDataBeans(idData, userIdData, statusNameData, updateDateData);
	
			statusList.add(status);
	        
			}
		      
	    } catch (SQLException e) {
	    	e.printStackTrace();
	    	return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	    return statusList;
	}
	

	public void updateStatusData(String statusId, String statusName) {
	    Connection conn = null;
	    int StatusId = Integer.parseInt(statusId);
	    
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "UPDATE status SET status_nm = ?, update_date = now() WHERE id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, statusName);
	        pStmt.setInt(2, StatusId);
	    
	        pStmt.executeUpdate();
	       	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}

}
	

