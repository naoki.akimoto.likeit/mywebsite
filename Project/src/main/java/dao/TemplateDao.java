package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.TemplateDataBeans;

public class TemplateDao {
	
	public void insertTemplate(String routineId, String templateTitle, String templateContent) {
	    Connection conn = null;
	    int RoutineId = Integer.parseInt(routineId);
	    
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "INSERT INTO template (routine_id, template_title, template_content, update_date) VALUES ( ?, ?, ?, now())";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, RoutineId);
	        pStmt.setString(2, templateTitle);
	        pStmt.setString(3, templateContent);

	        pStmt.executeUpdate();       
	        
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	public TemplateDataBeans findTemplateData(String routineId) {
	    Connection conn = null;
	    int RoutineId = Integer.parseInt(routineId);
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();
	
	        // SELECT文を準備
	        String sql = "SELECT * FROM template WHERE routine_id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, RoutineId);
	        ResultSet rs = pStmt.executeQuery();
	
	        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	        if (!rs.next()) {
	            return null;
	        }
	
	        // 必要なデータのみインスタンスのフィールドに追加
	        int idData = rs.getInt("id");
	        int routineIdData = rs.getInt("routine_id");
	        String templateTitleData = rs.getString("template_title");
	        String templateContentData = rs.getString("template_content");
	        String updateDateData = rs.getString("update_date");
	
	        return new TemplateDataBeans(idData, routineIdData, templateTitleData, templateContentData, updateDateData);
	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }
	}

	public void updateTemplateData(int routineId, String templateTitle, String templateContent) {
	    Connection conn = null;
//	    int RoutineId = Integer.parseInt(routineId);
	    
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "UPDATE template SET template_title = ?, template_content = ?, update_date = now() WHERE routine_id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, templateTitle);
	        pStmt.setString(2, templateContent);
	        pStmt.setInt(3, routineId);
	    
	        pStmt.executeUpdate();
	       	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}

}
