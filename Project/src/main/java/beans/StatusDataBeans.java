package beans;

public class StatusDataBeans {
	
	private int id;
	private int userId;
	private String statusName;
	private String updateDate;
	
	public StatusDataBeans() {
		
	}

	public StatusDataBeans(int id, int userId, String statusName, String updateDate) {
		
		this.id = id;
		this.userId = userId;
		this.statusName = statusName;
		this.updateDate = updateDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	

}
