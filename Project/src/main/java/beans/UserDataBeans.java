package beans;

import java.io.Serializable;

public class UserDataBeans implements Serializable {
	
	private int id;
	private String loginId;
	private String name;
	private String password;
	private String createDate;
	private String updateDate;
	
	public UserDataBeans() {
		
	}
	
	public UserDataBeans(int id) {
		this.id = id;
	}
	
	public UserDataBeans(int id, String loginId, String name) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
	}
	
	public UserDataBeans(int id, String loginId, String name, String password, String createDate, String updateDate) {
		super();
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateData() {
		return createDate;
	}
	public void setCreateData(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateData() {
		return updateDate;
	}
	public void setUpdateData(String updateDate) {
		this.updateDate = updateDate;
	}
		
	
	
		
	
	

}
