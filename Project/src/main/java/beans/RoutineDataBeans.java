package beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RoutineDataBeans {
	
	private int id;
	private int userId;
	private int statusId;
	private String routineName;
	private String period;
	private String effects;
	private String createDate;
	private String updateDate;
	
	public RoutineDataBeans() {
		
	}

	public RoutineDataBeans(int id, int userId, int statusId, String routineName, String period, String effects,
			String createDate, String updateDate) {
		super();
		this.id = id;
		this.userId = userId;
		this.statusId = statusId;
		this.routineName = routineName;
		this.period = period;
		this.effects = effects;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getRoutineName() {
		return routineName;
	}

	public void setRoutineName(String routineName) {
		this.routineName = routineName;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}
	
	public String getEffects() {
		return effects;
	}

	public void setEffects(String effects) {
		this.effects = effects;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getFormatDate() {
		
		String str = getCreateDate();
	    
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			Date date = sdf.parse(str);
			System.out.println(date.toString());
			
			SimpleDateFormat f = new SimpleDateFormat("yyyy年MM月dd日 HH時mm分");
			String dateStr = f.format(date);
			System.out.println(dateStr);
			
			return dateStr;
			
			} catch(ParseException e) {
				e.printStackTrace();
				
				return null;
				
			}
		
	}




}
