package beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RecordDataBeans {
	
	private int id;
	private int routineId;
	private String recordTitle;
	private String recordContent;
	private String createDate;
	private String updateDate;
	
	public RecordDataBeans() {
		// TODO 自動生成されたメソッド・スタブ
	}

	public RecordDataBeans(int id, int routineId, String recordTitle, String recordContent, String createDate,
			String updateDate) {
		
		this.id = id;
		this.routineId = routineId;
		this.recordTitle = recordTitle;
		this.recordContent = recordContent;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRoutineId() {
		return routineId;
	}

	public void setRoutineId(int routineId) {
		this.routineId = routineId;
	}

	public String getRecordTitle() {
		return recordTitle;
	}

	public void setRecordTitle(String recordTitle) {
		this.recordTitle = recordTitle;
	}

	public String getRecordContent() {
		return recordContent;
	}

	public void setRecordContent(String recordContent) {
		this.recordContent = recordContent;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	
	public String getFormatDate() {
		
		String str = getCreateDate();
	    
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			Date date = sdf.parse(str);
			System.out.println(date.toString());
			
			SimpleDateFormat f = new SimpleDateFormat("yyyy年MM月dd日 HH時mm分");
			String dateStr = f.format(date);
			
			return dateStr;
			
			} catch(ParseException e) {
				e.printStackTrace();
				
				return null;
				
			}
		
	}
	
	

}
