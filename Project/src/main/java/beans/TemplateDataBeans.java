package beans;

import java.io.Serializable;

public class TemplateDataBeans implements Serializable {
	
	private int id;
	private int routineId;
	private String templateTitle;
	private String templateContent;
	private String updateDate;
	
	
	public TemplateDataBeans() {
		
	}

	public TemplateDataBeans(int id, int routineId, String templateTitle, String templateContent, String updateDate) {
		
		this.id = id;
		this.routineId = routineId;
		this.templateTitle = templateTitle;
		this.templateContent = templateContent;
		this.updateDate = updateDate;
	}




	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getRoutineId() {
		return routineId;
	}


	public void setRoutineId(int routineId) {
		this.routineId = routineId;
	}


	public String getTemplateTitle() {
		return templateTitle;
	}


	public void setTemplateTitle(String templateTitle) {
		this.templateTitle = templateTitle;
	}


	public String getTemplateContent() {
		return templateContent;
	}


	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}


	public String getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	
	
	
}
