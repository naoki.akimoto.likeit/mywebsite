package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.StatusDataBeans;
import beans.UserDataBeans;
import dao.StatusDao;

/**
 * Servlet implementation class StatusUpdate
 */
@WebServlet("/StatusUpdate")
public class StatusUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StatusUpdate() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//未ログイン時の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		//ステータスリストを取得
		@SuppressWarnings("unchecked")
		List<StatusDataBeans> statusList = (List<StatusDataBeans>)session.getAttribute("statusList");
		
		if(loginUser == null || statusList == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
		
//		int userId = loginUser.getId();
////		情報をセット
//		StatusDao statusDao = new StatusDao();
		
//		/** テーブルに該当のデータが見つからなかった場合 **/
//		if (statusList == null) {
//			// テンプレート初期値を設定
//			String statusName1 = "継続中の習慣";
//			String statusName2 = "取り入れた習慣";
//			String statusName3 = "その他";
//			String statusName4 = "ゴミ箱";
//			
//			statusDao.insertStatus(userId,statusName1);
//			statusDao.insertStatus(userId,statusName2);
//			statusDao.insertStatus(userId,statusName3);
//			statusDao.insertStatus(userId,statusName4);
//
//			statusList = statusDao.findStatusListData(userId);
//			request.setAttribute("statusList", statusList);
//		}
		
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/statusUpdate.jsp");
		dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		//ログインIDを取得
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		int userId = loginUser.getId();

		// リクエストパラメータの入力項目を取得
		String statusName1 = request.getParameter("statusName0");
		String statusId1 = request.getParameter("statusId0");

		String statusName2 = request.getParameter("statusName1");
		String statusId2 = request.getParameter("statusId1");
		
		String statusName3 = request.getParameter("statusName2");
		String statusId3 = request.getParameter("statusId2");

		
		StatusDao statusDao = new StatusDao();
		statusDao.updateStatusData(statusId1,statusName1);
		statusDao.updateStatusData(statusId2,statusName2);
		statusDao.updateStatusData(statusId3,statusName3);
		
		List<StatusDataBeans> statusList = statusDao.findStatusListData(userId);
		session.setAttribute("statusList", statusList);

		response.sendRedirect("Index");
	
	}

}
