package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RoutineDataBeans;
import beans.UserDataBeans;
import dao.RoutineDao;

/**
 * Servlet implementation class RoutineDelete
 */
@WebServlet("/RoutineDelete")
public class RoutineDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RoutineDelete() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		if(loginUser == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
		
		String id = request.getParameter("id");
		
		RoutineDao routineDao = new RoutineDao();
		RoutineDataBeans routine = routineDao.findRoutineData(id);
		
		request.setAttribute("routineData", routine);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/routineDelete.jsp");
		dispatcher.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    
		String id = request.getParameter("id");
		
		RoutineDao routineDao = new RoutineDao();
		routineDao.deleteroutineData(id);
		
		response.sendRedirect("Index");	
	}
	
	
	

}
