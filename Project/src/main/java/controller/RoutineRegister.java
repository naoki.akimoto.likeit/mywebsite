package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.StatusDataBeans;
import beans.UserDataBeans;
import dao.RoutineDao;

/**
 * Servlet implementation class RoutineRegister
 */
@WebServlet("/RoutineRegister")
public class RoutineRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RoutineRegister() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//未ログイン時の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		//ステータスリストを取得
		@SuppressWarnings("unchecked")
		List<StatusDataBeans> statusList = (List<StatusDataBeans>)session.getAttribute("statusList");
		
		if(loginUser == null || statusList == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
		
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/routineRegister.jsp");
		dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		
		//ログイン中のUserIdを取得
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		int UserId = loginUser.getId();

		// リクエストパラメータの入力項目を取得
		String title = request.getParameter("routineTitle");
		String status = request.getParameter("routineStatus");
		String period = request.getParameter("routinePeriod");
		String effects = request.getParameter("routineEffects");
		
        int Status = Integer.parseInt(status);

		/** 空欄があった場合 **/
		if (title.equals("")) {
//			リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "題名を入力してください。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/routineRegister.jsp");
			dispatcher.forward(request, response);
			
		} else {

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		RoutineDao routineDao = new RoutineDao();
		routineDao.insertRoutine(title, period, effects, UserId, Status);
		
		//リダイレクト
		response.sendRedirect("Index");
		}
	}

}
