package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//未ログイン時の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		if(loginUser == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
		
		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		UserDataBeans user = userDao.findUserData(id);
		
		session.setAttribute("userData", user);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		
		if (!(password.equals(checkPassword)) && !(checkPassword.equals(""))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワードが一致しません。");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}
		
		/** 空欄があった場合 **/
		if (loginId.equals("") || name.equals("")) {
//			 リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワード以外の項目を埋めてください。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}
	
		UserDao userDao = new UserDao();
		
		// パスワードがない場合、オーバーロード？
		if (password.equals("")||checkPassword.equals("")) {
			userDao.updateUserData(name, loginId);
			
		} else {
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";
	
			//ハッシュ生成処理
			byte[] bytes = null ;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			
			String insertPassword = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			System.out.println(insertPassword);
			
			userDao.updateUserData(insertPassword, name, loginId);
		}
		
		HttpSession session = request.getSession();
		session.removeAttribute("userData");
		
		response.sendRedirect("Index");
	
	}

}
