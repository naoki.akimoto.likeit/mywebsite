package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RecordDataBeans;
import beans.RoutineDataBeans;
import beans.UserDataBeans;
import dao.RecordDao;
import dao.RoutineDao;

/**
 * Servlet implementation class RecordUpdate
 */
@WebServlet("/RecordUpdate")
public class RecordUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecordUpdate() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		未ログイン時の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		if(loginUser == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
		
		//ボタンからidを取得
		String id = request.getParameter("id");
		
		RecordDao recordDao = new RecordDao();
		RecordDataBeans record = recordDao.findRecordData(id);
		//日時を○○年○月○日に変換
		record.setCreateDate(record.getFormatDate());
		session.setAttribute("recordData", record);
		
		//ルーティンの情報取得
		int routineId = record.getRoutineId();
		RoutineDao routineDao = new RoutineDao();
		RoutineDataBeans routine = routineDao.findRoutineDataByIntId(routineId);

		request.setAttribute("routineData", routine);

		// jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recordUpdate.jsp");
		dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		RecordDataBeans record =(RecordDataBeans)session.getAttribute("recordData");

		int recordId = record.getId();
		
		// リクエストパラメータの入力項目を取得
		String recordTitle = request.getParameter("recordTitle");
		String recordContent = request.getParameter("recordContent");
		
		RecordDao recordDao = new RecordDao();
		recordDao.updateRecordData(recordId,recordTitle,recordContent);
		
		session.removeAttribute("recordData");

		//リダイレクト
		response.sendRedirect("RecordDetail?id=" + recordId);
	
	}

}
