package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RoutineDataBeans;
import beans.TemplateDataBeans;
import beans.UserDataBeans;
import dao.RoutineDao;
import dao.TemplateDao;

/**
 * Servlet implementation class RecordTemplate
 */
@WebServlet("/RecordTemplate")
public class RecordTemplate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecordTemplate() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//未ログイン時の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		if(loginUser == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
		
		String routineId = request.getParameter("id");
//		クリックしたルーティンの情報をセット
		RoutineDao routineDao = new RoutineDao();
		RoutineDataBeans routine = routineDao.findRoutineData(routineId);
		session.setAttribute("routineData", routine);
		
//		テンプレートの情報をセット
		TemplateDao templateDao = new TemplateDao();
		TemplateDataBeans template = templateDao.findTemplateData(routineId);
		request.setAttribute("templateData", template);
		
		/** テーブルに該当のデータが見つからなかった場合 **/
		if (template == null) {
			// テンプレート初期値を設定
			String templateTitle = "";
			String templateContent = "";
			templateDao.insertTemplate(routineId,templateTitle,templateContent);
			template = templateDao.findTemplateData(routineId);
			
			request.setAttribute("templateData", template);
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recordTemplate.jsp");
		dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		
		//ログイン中のUserIdを取得
		HttpSession session = request.getSession();
		RoutineDataBeans routine =(RoutineDataBeans)session.getAttribute("routineData");
		
		int routineId = routine.getId(); 
		
		// リクエストパラメータの入力項目を取得
		String templateTitle = request.getParameter("templateTitle");
		String templateContent = request.getParameter("templateContent");
		
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		TemplateDao templateDao = new TemplateDao();
		templateDao.updateTemplateData(routineId, templateTitle, templateContent);
		
		//セッションを削除
		session.removeAttribute("routineData");
		
		//リダイレクト
		response.sendRedirect("RoutineDetail?id=" + routineId);	
	
	}

}
