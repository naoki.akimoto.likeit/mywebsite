package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RecordDataBeans;
import beans.RoutineDataBeans;
import beans.StatusDataBeans;
import beans.UserDataBeans;
import dao.RecordDao;
import dao.RoutineDao;

/**
 * Servlet implementation class RoutineDetail
 */
@WebServlet("/RoutineDetail")
public class RoutineDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RoutineDetail() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//未ログイン時の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		//ステータスリストを取得
		@SuppressWarnings("unchecked")
		List<StatusDataBeans> statusList = (List<StatusDataBeans>)session.getAttribute("statusList");
		
		if(loginUser == null || statusList ==  null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
		
		String id = request.getParameter("id");
		System.out.println(id);
		
		//クリックしたルーティンの情報をセット
		RoutineDao routineDao = new RoutineDao();
		RoutineDataBeans routine = routineDao.findRoutineData(id);
		
		//日時を○○年○月○日に変換
		routine.setCreateDate(routine.getFormatDate());
		request.setAttribute("routineData", routine);
		
		//recordリストをセット
		RecordDao recordDao = new RecordDao();
		List<RecordDataBeans> recordList = recordDao.findRecordLsit(id);
		request.setAttribute("recordList", recordList);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/routineDetail.jsp");
		dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// POST処理はなし。全てaタブ。
		doGet(request, response);
	}

}
