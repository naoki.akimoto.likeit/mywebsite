package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RecordDataBeans;
import beans.UserDataBeans;
import dao.RecordDao;

/**
 * Servlet implementation class RecordDelete
 */
@WebServlet("/RecordDelete")
public class RecordDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecordDelete() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		未ログイン時の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		if(loginUser == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
		
		//ボタンからidを取得
		String id = request.getParameter("id");
		
		RecordDao recordDao = new RecordDao();
		RecordDataBeans record = recordDao.findRecordData(id);
		session.setAttribute("recordData", record);
		
		// jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recordDelete.jsp");
		dispatcher.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		RecordDataBeans record =(RecordDataBeans)session.getAttribute("recordData");

		int recordId = record.getId();
		int routineId = record.getRoutineId();
		
		RecordDao recordDao = new RecordDao();
		recordDao.deleteRecordData(recordId);
		
		session.removeAttribute("recordData");

		//リダイレクト
		response.sendRedirect("RoutineDetail?id=" + routineId);
		
	}

}
