package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RoutineDataBeans;
import beans.TemplateDataBeans;
import beans.UserDataBeans;
import dao.RecordDao;
import dao.RoutineDao;
import dao.TemplateDao;

/**
 * Servlet implementation class RecordRegister
 */
@WebServlet("/RecordRegister")
public class RecordRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecordRegister() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		未ログイン時の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		if(loginUser == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
		
		//ボタンからidを取得
		String id = request.getParameter("id");
		
		//Routineの情報をセット
		RoutineDao routineDao = new RoutineDao();
		RoutineDataBeans routine = routineDao.findRoutineData(id);
		session.setAttribute("routineData", routine);
		
		//テンプレートの情報をセット
		TemplateDao templateDao = new TemplateDao();
		TemplateDataBeans template = templateDao.findTemplateData(id);
		request.setAttribute("templateData", template);
		
		// jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recordRegister.jsp");
		dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		
		//rouitneIdを取得
		HttpSession session = request.getSession();
		RoutineDataBeans routine =(RoutineDataBeans)session.getAttribute("routineData");
		
		int routineId = routine.getId(); 
		System.out.println(routineId);
		
		// リクエストパラメータの入力項目を取得
		String recordTitle = request.getParameter("recordTitle");
		String recordContent = request.getParameter("recordContent");
		
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		RecordDao recordDao = new RecordDao();
		recordDao.insertRecord(routineId, recordTitle, recordContent);
		
		//セッションを削除
		session.removeAttribute("routineData");
		
		//リダイレクト
		response.sendRedirect("RoutineDetail?id=" + routineId);	
	}

}
