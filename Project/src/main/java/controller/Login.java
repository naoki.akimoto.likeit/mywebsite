package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import beans.StatusDataBeans;
import beans.UserDataBeans;
import dao.StatusDao;
import dao.UserDao;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		//ログイン済の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		
		if(loginUser != null) {
			response.sendRedirect("Index");
			
		} else {
			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null ;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String insertPassword = DatatypeConverter.printHexBinary(bytes);
		System.out.println(insertPassword);
		
		UserDataBeans user = userDao.findByLoginInfo(loginId, insertPassword);

		/** テーブルに該当のデータが見つからなかった場合 **/
		if (user == null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ログインに失敗しました。");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}

		/** テーブルに該当のデータが見つかった場合 **/
		// セッションにユーザの情報をセット
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);
		
		int userId = user.getId();
		//情報をセット
		StatusDao statusDao = new StatusDao();
		List<StatusDataBeans> statusList = statusDao.findStatusListData(userId);
		
		/** テーブルに該当のデータが見つからなかった場合 **/
		if (statusList.size() == 0) {
			
			// テンプレート初期値を設定
			String statusName1 = "継続中の習慣";
			String statusName2 = "取り入れた習慣";
			String statusName3 = "その他";
			String statusName4 = "ゴミ箱";
			
			statusDao.insertStatus(userId,statusName1);
			statusDao.insertStatus(userId,statusName2);
			statusDao.insertStatus(userId,statusName3);
			statusDao.insertStatus(userId,statusName4);
			
			System.out.println("ステータス新規登録");

			statusList = statusDao.findStatusListData(userId);
			
		} else {
			System.out.println("ステータスID確認");
			for(StatusDataBeans statusData : statusList){
	            System.out.println(statusData.getId());
			}
		}
		
		session.setAttribute("statusList", statusList);
		
		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("Index");
	}

}
