package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RoutineDataBeans;
import beans.StatusDataBeans;
import beans.UserDataBeans;
import dao.RoutineDao;

/**
 * Servlet implementation class RoutineUpdate
 */
@WebServlet("/RoutineUpdate")
public class RoutineUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RoutineUpdate() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//未ログイン時の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		//ステータスリストを取得
		@SuppressWarnings("unchecked")
		List<StatusDataBeans> statusList = (List<StatusDataBeans>)session.getAttribute("statusList");
		
		if(loginUser == null || statusList == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
		
		//ボタンからidを取得
		String id = request.getParameter("id");

		RoutineDao routineDao = new RoutineDao();
		RoutineDataBeans routine = routineDao.findRoutineData(id);
		
		session.setAttribute("routineData", routine);
		
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/routineUpdate.jsp");
		dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		
		//Idを取得
		HttpSession session = request.getSession();
		RoutineDataBeans routine =(RoutineDataBeans)session.getAttribute("routineData");
		
		int Id = routine.getId(); 

		// リクエストパラメータの入力項目を取得
		String title = request.getParameter("routineTitle");
		String status = request.getParameter("routineStatus");
		String period = request.getParameter("routinePeriod");
		String effects = request.getParameter("routineEffects");
		
        int Status = Integer.parseInt(status);
        
        System.out.println(Id);
		System.out.println(status);

		/** 空欄があった場合 **/
		if (title.equals("")) {
//					 リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "習慣タイトルを入力してください。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/routineUpdate.jsp");
			dispatcher.forward(request, response);
			
		} else {
			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			RoutineDao routineDao = new RoutineDao();
			routineDao.updateRoutineData(Id, Status, title, period, effects);
			
			//セッションを削除
			session.removeAttribute("routineData");
			
			//リダイレクト
			response.sendRedirect("RoutineDetail?id=" + Id);
		}
		
	}

}
