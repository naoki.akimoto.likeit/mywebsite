package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserRegister
 */
@WebServlet("/UserRegister")
public class UserRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegister() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//未ログイン時の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		if(loginUser != null) {
			response.sendRedirect("Index");
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userRegister.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String name = request.getParameter("name");

		/** 空欄があった場合 **/
		if (loginId.equals("") || password.equals("") || name.equals("")) {
//			 リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "項目を全て埋めてください。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userRegister.jsp");
			dispatcher.forward(request, response);
		}

		/** パスワードが確認用と一致しなかった場合 **/
		if (!(password.equals(checkPassword))) {
//				 リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワードが一致しません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userRegister.jsp");
			dispatcher.forward(request, response);
		}

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		UserDataBeans user = userDao.confirmUserId(loginId);

		if (user == null) {
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";
	
			//ハッシュ生成処理
			byte[] bytes = null ;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			
			String insertPassword = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			System.out.println("パスワードを暗号化:" + insertPassword);

			userDao.insertUser(loginId, insertPassword, name);
			
			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("Index");

		} else {
			request.setAttribute("errMsg", "既にこのユーザーIDは使用されております。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userRegister.jsp");
			dispatcher.forward(request, response);
		}

	}

}
