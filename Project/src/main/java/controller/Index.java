package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RoutineDataBeans;
import beans.StatusDataBeans;
import beans.UserDataBeans;
import dao.RoutineDao;

/**
 * Servlet implementation class Index
 */
@WebServlet("/Index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
//		未ログイン時の処理
		HttpSession session = request.getSession();
		UserDataBeans loginUser =(UserDataBeans)session.getAttribute("userInfo");
		
		//ステータスリストを取得
		@SuppressWarnings("unchecked")
		List<StatusDataBeans> statusList = (List<StatusDataBeans>)session.getAttribute("statusList");
		
		if(loginUser == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			
		} else if (loginUser != null && statusList.size() == 0) {
			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("Logout");
			
		} else if (loginUser != null && statusList.size() != 0){
			//ログインユーザIDを取得
			int userId = loginUser.getId();
			// ステータス１のリストを取得
			RoutineDao routineDao = new RoutineDao();
			List<RoutineDataBeans> routineStatus1List = routineDao.findRoutineByStatus(userId, statusList.get(0).getId());
			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("routineStatus1List", routineStatus1List);
			
			// ステータス2のリストを取得
			List<RoutineDataBeans> routineStatus2List = routineDao.findRoutineByStatus(userId, statusList.get(1).getId());
			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("routineStatus2List", routineStatus2List);
			
			// ステータス3のリストを取得
			List<RoutineDataBeans> routineStatus3List = routineDao.findRoutineByStatus(userId, statusList.get(2).getId());
			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("routineStatus3List", routineStatus3List);
			
			// ユーザ一覧のjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);
		} else {
			// ユーザ一覧のjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
