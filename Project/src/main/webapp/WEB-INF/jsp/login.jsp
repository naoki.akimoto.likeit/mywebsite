
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8">
    <title>Syuukann ログイン</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link type="text/css" href="<%=request.getContextPath() %>/css/stylesheet.css" rel="stylesheet">
  </head>

	<body>
	
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

    <div class = "wrapper">

      <div class = "login-heading">
        <h1>Syuukann</h1>
        <p>〜 継続こそチカラ 〜</p>
  
      </div>
  
      <div class = "container">
  
        <form action = "Login" method = "post">
  
          <div class="form-group">
            <label for="exampleInputEmail1">ログインID</label>
            <input type="text" class="form-control" id="login_id" 
            name = "loginId" aria-describedby="emailHelp" placeholder="半角英数字">
           <!--  <small id="emailHelp" class="form-text text-muted"></small> -->
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">パスワード</label>
            <input type="password" class="form-control" id="password" 
            name = "password" aria-describedby="emailHelp" placeholder="半角英数字">
          </div>

          <button type="submit" class="btn btn-primary">ログイン</button>
        </form>
  
        <a href="UserRegister"> 新規登録はこちらから </a>
  
  
      </div>

    </div>
		
    

	</body>
</html>
