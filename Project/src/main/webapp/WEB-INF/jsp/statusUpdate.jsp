<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Syuukann ステータス名変更</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<link type="text/css"
	href="<%=request.getContextPath()%>/css/stylesheet.css"
	rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
	integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>

</head>

<body>

	<header>

		<a class="header-logo" href="Index" role="button">Syuukann</a>

		<div class="header-list">
			<ul>

				<li><a href="Logout" data-toggle="tooltip"
					data-placement="bottom" title="ログアウト"> <i
						class="fas fa-sign-out-alt"></i>
				</a></li>

				<li><a href="TrashBox" data-toggle="tooltip"
					data-placement="bottom" title="ゴミ箱"> <i class="fas fa-trash"></i>
				</a></li>

				<li><a href="StatusUpdate?id=${userInfo.id}"
					data-toggle="tooltip" data-placement="bottom" title="ステータス名変更">
						<i class="fas fa-wrench"></i>
				</a></li>

				<li><a href="UserUpdate?id=${userInfo.id}"
					data-toggle="tooltip" data-placement="bottom" title="ユーザ情報"> <i
						class="fas fa-user-circle"></i>
				</a></li>

				<li><a href="RoutineRegister" data-toggle="tooltip"
					data-placement="bottom" title="新規習慣作成"> <i
						class="far fa-calendar-plus"></i>
				</a></li>

			</ul>
		</div>

	</header>

	<div class="wrapper">

		<div class="heading">
			<h1>ステータス名変更</h1>
			<p></p>
		</div>

		<div class="container">

			<form action = "StatusUpdate" method = "post">

				<c:forEach var="status" items="${statusList}" varStatus="i" begin="0" end ="2">

					<div class="form-group">
						<label for="exampleInputEmail1">ステータス名</label> 
						<input type="text" name = "statusName${i.index}"
							class="form-control" id="status" value="${status.statusName}">
						<input type="hidden" name=statusId${i.index} value="${status.id}">
					</div>

				</c:forEach>

				<div class="comment">
					<p>※メインページでは上から順に表示されます。</p>
				</div>

				<button type="submit" class="btn btn-primary">更新</button>
			</form>



		</div>
	</div>

	<script type="text/javascript">
		// タグ内にjavascriptコードを直接記述します。
		$('[data-toggle="tooltip"]').tooltip();
	</script>


</body>
</html>