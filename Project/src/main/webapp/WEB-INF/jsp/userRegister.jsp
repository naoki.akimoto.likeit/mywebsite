
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8">
    <title>新規登録</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link type="text/css" href="<%=request.getContextPath() %>/css/stylesheet.css" rel="stylesheet">
  </head>

  <body>
  
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>  

    <div class = "wrapper">

      <div class = "heading">
        <h1>新規登録</h1>
        
  
      </div>
  
      <div class = "container">
  
        <form action = "UserRegister" method = "post">
  
          <div class="form-group">
            <label for="exampleInputEmail1">ログインID</label>
            <input type="text" class="form-control" id="login_id" 
            name = "loginId" aria-describedby="emailHelp" placeholder="半角英数字">
          </div>
  
          <div class="form-group">
            <label for="exampleInputEmail1">ユーザー名</label>
            <input type="text" class="form-control" id="name" 
            name = "name" aria-describedby="emailHelp" placeholder="半角英数字">
            <small id="emailHelp" class="form-text text-muted"></small>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">パスワード</label>
            <input type="password" class="form-control" id="password" 
            name = "password" aria-describedby="emailHelp" placeholder="半角英数字">
            <small id="emailHelp" class="form-text text-muted"></small>
          </div>


          <div class="form-group">
            <label for="exampleInputPassword1">パスワード（確認用）</label>
            <input type="password" class="form-control" id="password_check" 
            name = "checkPassword" placeholder="半角英数字">
          </div>


          
          <button type="submit" class="btn btn-primary">登録</button>
        </form>
  
  
      </div>

    </div>
		
    <script type="text/javascript">
      // タグ内にjavascriptコードを直接記述します。
      $('[data-toggle="tooltip"]').tooltip();
    </script>
    

	</body>


  </html>