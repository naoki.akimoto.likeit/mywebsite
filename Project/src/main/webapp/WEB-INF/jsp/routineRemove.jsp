
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Syuukann 習慣をゴミ箱に移動</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<link type="text/css"
	href="<%=request.getContextPath()%>/css/stylesheet.css"
	rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
	integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>

</head>

<body>

	<header>

		<a class="header-logo" href="index.html" role="button">Syuukann</a>


		<div class="header-list">
			<ul>

				<li><a href="login.html" data-toggle="tooltip"
					data-placement="bottom" title="ログアウト"> <i
						class="fas fa-sign-out-alt"></i>
				</a></li>

				<li><a href="trashBox.html" data-toggle="tooltip"
					data-placement="bottom" title="ゴミ箱"> <i class="fas fa-trash"></i>
				</a></li>

				<li><a href="StatusUpdate.html" data-toggle="tooltip"
					data-placement="bottom" title="ステータス名変更"> <i
						class="fas fa-wrench"></i>
				</a></li>

				<li><a href="userUpdate.html" data-toggle="tooltip"
					data-placement="bottom" title="ユーザ情報"> <i
						class="fas fa-user-circle"></i>
				</a></li>

				<li><a href="routineRegister.html" data-toggle="tooltip"
					data-placement="bottom" title="新規習慣作成"> <i
						class="far fa-calendar-plus"></i>
				</a></li>

			</ul>
		</div>

	</header>

	<div class="wrapper">

		<div class="heading"></div>

		<div class="container">

			<div class="delete-heading">
				<p>
					習慣『習慣名がここに入る』<br>ゴミ箱にいれますか？
				</p>

			</div>

			<div class="a-left cancel">
				<a class="btn btn-secondary" href="routineDetail.html" role="button">キャンセル</a>
			</div>

			<form>

				<div class="a-right delete">
					<button type="submit" class="btn btn-danger">ゴミ箱に入れる</button>
				</div>

			</form>

		</div>

	</div>

	<script type="text/javascript">
		// タグ内にjavascriptコードを直接記述します。
		$('[data-toggle="tooltip"]').tooltip();
	</script>

</body>
</html>